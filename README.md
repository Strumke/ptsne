# pt-SNE :sunglasses:
**p**arametric **t**-**S**tochastic**N**eighbor **E**mbedding


# Usage
Run 
```r
python3 ptsne.py
```
Test data is provided.

<img src="example_result_20epochs.png" align="left" alt="" width="520" />