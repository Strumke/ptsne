import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import cv2

from make_plot import plot_scatter

def earth_movers_distance(source, target):
    def vec_to_sig(arr):
        sig = np.empty((arr.size, 2), dtype=np.float32)
        count = 0
        for i in range(arr.shape[0]):
            sig[count] = np.array([arr[ i ], i])
            count += 1
        return sig

    sig1 = vec_to_sig(source)
    sig2 = vec_to_sig(target)

    dist, _, _ = cv2.EMD(sig1, sig2, cv2.DIST_L2)

    return dist

def wasserstein_distance(source, target):
    return stats.wasserstein_distance(source, target)

def euclidean_distance(source, target):
    return np.sqrt(sum(np.abs(source - target)))

def n_smallest(arr, n):
    """ Returns indices of the n smalles elements of the numpy array 'arr'"""
    return arr.argsort()[:n]

def plot_result(train_embedding, train_labels, zoom=True):
    # --- Make universal color palette and plot all training embeddings
    num_clusters = len(set(train_labels))
    color_palette = sns.color_palette("hls", num_clusters)
    plot_scatter(train_embedding, train_labels, color_palette=color_palette, alpha=0.8)

    # --- Loop over neighboring classes and highlight neighbors
    for _l in set(n_labels):
        _rows = (n_labels==_l)
        _n_l = list(n_labels).count(_l)
        plot_scatter(n_embedded[_rows], n_labels[_rows],
                color_palette=color_palette, alpha=0.8, marker='*',
                label=f"{_n_l} neighbors. Cluster={_l}")

    # --- Highlight instance to explain on the plot
    # --- True color: color_palette[test_labels[pred_index]]
    plt.scatter(exp_pred[0], exp_pred[1], c="black", marker='o',
                s=20, label=f"Instance. True cluster={true_cluster}", zorder=3)

    if zoom:
        # --- Zoom in on plot
        x_range = [exp_pred[0]-10, exp_pred[0]+10]
        y_range = [exp_pred[1]-10, exp_pred[1]+10]
        plt.xlim(x_range)
        plt.ylim(y_range)

    plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
    plt.show()

if __name__ == "__main__":
    from ptsne_parameters import pred_index, n_neighbors, TARGET_DICT

    # --- Load embeddings (training and unseen)
    train_embedding = np.load("embeddings.npy")
    train_labels = np.load("train_labels.npy")
    test_embedding = np.load("preds.npy")
    test_labels = np.load("test_labels.npy")

    true_cluster = test_labels[pred_index]
    print(f"True cluster: {true_cluster}, {TARGET_DICT.get(true_cluster, true_cluster)}")

    # --- Prediction to explain
    exp_pred = test_embedding[pred_index]
    distances = np.array([euclidean_distance(_emb, exp_pred) for _emb in
        train_embedding])
    n_indices = n_smallest(distances, n_neighbors)
    n_embedded = train_embedding[n_indices]
    n_labels = train_labels[n_indices]

    plot_result(train_embedding, train_labels, zoom=False)
    plot_result(train_embedding, train_labels, zoom=True)
