"""
This code is based on
https://lvdmaaten.github.io/publications/papers/AISTATS_2009.pdf
Inga Strumke, 2020.

Some definitions:

Q_i is a student-t based joint probability distribution (see make_Q)
P_i is a Gaussian probability distribution centered over a data point x_i, with
width sigma_i (see make_P)

Perplexity is defined as 2**(H(P)), where H is the Shannon entroy
H(P_i) = sum_j ( p_j|i *log2 p_j|i ). This is always input by the user, and not
estimated.

The Kullback-Leibler divergence is
KL(P||Q)=sum(p_ij log(p_ij/Q_ij))

"""
import numpy as np
import pandas as pd
import sys
import seaborn as sns

from keras import models
from keras import layers
import functools
import tensorflow as tf
import tensorflow_probability as tfp

DEFAULT_EPS = 1e-8
DEFAULT_SIGMA_BATCH_SIZE = 1000
DEFAULT_TOLERANCE = 1e-4
DEFAULT_MAX_ITER = 100


def get_squared_diffs_np(sample):
    """
    Compute squared differences of sample data vectors using the vector product
    ||x_i - x_j||^2, where x_i = sample[i, :]
    Input:
    sample: (N, D) array
    Output:
    sq_diffs: (N, N) array
        Matrix of squared differences between x_i and x_j
    """
    batch_size = sample.shape[0]

    expanded = np.expand_dims(sample, 1)
    tiled = np.tile(expanded, np.stack([1, batch_size, 1]))
    diffs = tiled - np.transpose(tiled, axes=[1,0,2])
    sq_diffs = np.sum(np.square(diffs), axis=2)

    return sq_diffs


def make_P(samples, sigmas):
    """
    Transforms pairwise distances in the input sample space into probabilities
    by centering an isotropic Gaussian over each datapoint i, computing
    the density of point j under this Gaussian, and renormalisin to yield
    conditional probabilities P_ij.
    To form a single joint distribution, the conditional probabilities P_ij
    are symmetrised using get_normed_sym_np.
    Input:
    samples: 2d array_like, (N, D)
        Data samples
    sigmas: 1d array_like, (N,)
        Gaussian kernel widths
    Output:
    P_mat : nd array_like, (N, N)
        Symmetric joint probability matrix measuring the similarity between all
        datapoints
    """
    sq_diffs = get_squared_diffs_np(samples)
    # --- Each row in sq_diffs is multiplied by each sigma value
    prod = (sq_diffs * sigmas).T
    P_ji = np.exp(-1.0*prod)

    P_mat = get_normed_sym_np(P_ji)
    return P_mat

def make_train_generator(data, perplexity, batch_size):
    sigmas = estimate_widths(data, perplexity)
    steps = data.shape[0] // batch_size
    _step = -1
    while True:
        _step = (_step + 1) % steps
        _bounds = batch_size*_step, batch_size*(_step+1)
        _range = np.arange(_bounds[0], _bounds[1])
        _data = data[_range, :]
        _sigmas = sigmas[_range, :]

        _P_mat = make_P(_data, _sigmas)
        yield _data, _P_mat


def get_normed_sym_tf(input_matrix, batch_size):
    """
    Computes normalised and symmetrised probability matrix from
    relative probabilities matrix, where input_matrix is a Tensorflow tensor
    Input:
    input_matrix: (N, N) tensor
    Output:
    matrix: (N, N) tensor
        Symmetrised under the assumption that P(i|j) = P(j|i), with 0 diagonals
    """
    zeros = tf.constant(0, shape=[batch_size], dtype=input_matrix.dtype)
    matrix = tf.linalg.set_diag(input_matrix, zeros)
    norms = tf.reduce_sum(matrix, axis=0)
    matrix = matrix / norms # 1/N in eq ()
    matrix = 0.5*(matrix + tf.transpose(matrix))

    return matrix

def get_normed_sym_np(input_matrix, _eps=DEFAULT_EPS):
    """
    Compute the normalized and symmetrized probability matrix from
    relative probabilities matrix, where input_matrix is a numpy array
    Input:
    input_matrix: 2-d array_like (N, N)
        asymmetric probabilities. For instance, input_matrix(i, j) = P(i|j)
    Output:
    matrix: (N, N) array
        Symmetrised under the assumption that P(i|j) = P(j|i), with 0 diagonals
    """
    assert isinstance(input_matrix, np.ndarray)
    matrix = input_matrix.copy()

    batch_size = matrix.shape[0]
    zero_diags = 1.0 - np.identity(batch_size)
    matrix *= zero_diags
    norms = np.sum(matrix, axis=0, keepdims=True)
    matrix = matrix / (norms + _eps) # 1/N in eq()
    matrix = 0.5*(matrix + np.transpose(matrix))

    return matrix

def estimate_widths(sample, perplexity,
        sigma_batch_size=DEFAULT_SIGMA_BATCH_SIZE):
    """
    Calculate Gaussian fitted sigma (width) per data point
    Uses sigma_batch_size points at a time, to conserve memory
    Input:
    sample: (N, D) array
    perplexity: float
    sigma_batch_size : int, default=1000
    Output:
    sigma: (N, ) array
    """
    assert perplexity is not None

    num_pts = len(sample)
    training_sigmas = np.empty(num_pts)

    start = 0
    end = min(start+sigma_batch_size, num_pts)
    while start < num_pts:
        cur_training_data = sample[start:end, :]

        cur_training_sigmas, _, _ = estimate_sigmas_loop(cur_training_data,perplexity)
        training_sigmas[start:end] = cur_training_sigmas

        start += sigma_batch_size
        end = min(start+sigma_batch_size, num_pts)

    # --- Add dimension for later matrix multiplication
    training_sigmas = training_sigmas.reshape(-1,1)

    return training_sigmas

def get_squared_diffs_tf(sample):
    """
    Compute squared differences of sample data vectors using the vector product
    ||x_i - x_j||^2, where x_i = sample[i, :]
    Implementation for Tensorflow tensors
    Input:
    sample: (N, D) tensor
    Ouput:
    sq_diffs: (N, N) tensor
        Tensor of squared differences between x_i and x_j
    """
    batch_size = tf.shape(sample)[0]

    expanded = tf.expand_dims(sample, 1)
    tiled = tf.tile(expanded, tf.stack([1, batch_size, 1]))
    diffs = tiled - tf.transpose(tiled, perm=[1,0,2])
    sq_diffs = tf.reduce_sum(tf.square(diffs), axis=2)

    return sq_diffs

def make_Q(output, alpha, batch_size):
    """
    Calculate the student-t based probability distribution of the output
    Input:
    output: (N, output_dims) tensor
        Network output
    alpha: float
        `alpha` parameter. Recommend `output_dims` - 1.0
    batch_size: int (output.shape[0], but must be explicitly given)
    Output:
    Q_mat : (N, N) tensor
    """
    out_sq_diffs = get_squared_diffs_tf(output)
    Q_mat = tf.pow((1 + out_sq_diffs/alpha), -(alpha+1)/2)
    Q_mat = get_normed_sym_tf(Q_mat, batch_size)
    return Q_mat

def kl_loss(y_true, y_pred, alpha=1.0, batch_size=None, _eps=DEFAULT_EPS):
    """
    Kullback-Leibler loss function in Tensorflow, used during training.
    In our case, y_true is the Gaussian P matrix calculated from the input data, and
    y_pred is the network's lower dimensional output, from which we first
    calculate the t-distribution Q matrix.
    Input:
    y_true : (N, N) array
    y_pred : (N, output_dims) array
    alpha : float, optional
        Parameter used to calculate Q. Default 1.0
    batch_size : int
        Provided by training process.
    Output:
    kl_loss : tf.Tensor, scalar value
        Kullback-Leibler divergence KL(P||Q)=sum(p_ij log(p_ij/Q_ij))

    """
    batch_size = y_true.shape[0]
    P_mat = y_true
    Q_mat = make_Q(y_pred, alpha, batch_size)

    #X = tfp.distributions.Categorical(probs=y_true)
    #Y = tfp.distributions.Categorical(probs=y_pred)
    #print(tfp.distributions.kl_divergence(X, Y))

    _eps_tf = tf.constant(_eps, dtype=P_mat.dtype)

    kl_matr = tf.math.multiply(P_mat,
            tf.math.log(P_mat + _eps_tf) - tf.math.log(Q_mat + _eps_tf),
            name='kl_matr')
    zeros = tf.constant(0, shape=[batch_size], dtype=kl_matr.dtype)
    kl_matr_keep = tf.linalg.set_diag(kl_matr, zeros)
    kl_total_cost = tf.reduce_sum(kl_matr_keep)

    return kl_total_cost

def Hbeta_scalar(distances, sigma):
    """
    Computes the Gaussian kernel values from a list of squared Euclidean
    distances, and a Gaussian kernel width.
    Adapted from the matlab code in lvdmaaten.github.io/tsne
    Input:
    distances: (N, ) array
        Distance between current and remaining data points
    sigma: float
        Short for (2 sigma**2)^-1, where sigma is the ordinary Gaussian width
    Output:
    entropy: float
        Entropy of the current point
    P_mat: (N, ) array
        List of probabilities
        P_j|i = np.exp(-d*sigma)/sum(np.exp(-d*sigma))
    """
    P_mat = np.exp(-distances * sigma)
    sum_P = np.sum(P_mat)
    entropy = np.log(sum_P) + (sigma* np.sum(distances * P_mat)) / sum_P
    P_mat = P_mat / sum_P

    return entropy, P_mat

def estimate_sigmas_loop(data, perplexity,
        tolerance=DEFAULT_TOLERANCE, max_iter=DEFAULT_MAX_ITER):
    """
    Calculate sigma values given a perplexity
    Input:
    data: (N, D) array
    perplexity: float
    tolerance: float, default 1e-4
        The search concludes once difference in entropy stabilises by this
        amount, or when reaching max_iter
    max_iter: int, default=100
    Output:
    sigmas : (N, ) array
        Estimated gaussian widths, short for (2 sigma**2)^-1
    entropies: (N, ) array
    P_mat: (N, N) array
        Square probability matrix, see Hbeta_scalar
    """
    n_samples = data.shape[0]

    log_perp = np.log(perplexity)
    ones = np.ones([n_samples], dtype=float)
    sigmas = ones.copy()
    entropies = ones.copy()
    P_mat = np.zeros([n_samples, n_samples])

    sq_diffs = get_squared_diffs_np(data)

    for _sample in range(n_samples,):
        # --- Restart entropy difference for each sample
        e_diff = 100*tolerance
        # --- Start at full range (-inf, inf)
        min_sigma = -np.inf
        max_sigma = np.inf

        _distance = sq_diffs[_sample, :]
        _, _P_mat = Hbeta_scalar(_distance, sigmas[_sample])

        _iter = 0
        while abs(e_diff) > tolerance and _iter < max_iter:
            # --- Recompute entropy, probability and difference for the current
            # --- width estimate
            _entropy, _P_mat = Hbeta_scalar(_distance, sigmas[_sample])
            e_diff = _entropy - log_perp
            _iter += 1

            # --- If not within torerance, increase or decrease estimate
            if e_diff > 0.0:
                min_sigma = sigmas[_sample]
                if np.isinf(max_sigma):
                    sigmas[_sample] = 2.0*sigmas[_sample]
                else:
                    sigmas[_sample] = 0.5*(sigmas[_sample] + max_sigma)
            else:
                max_sigma = sigmas[_sample]
                if np.isinf(min_sigma):
                    sigmas[_sample] = 0.5*sigmas[_sample]
                else:
                    sigmas[_sample] = 0.5*(sigmas[_sample] + min_sigma)


        # --- Last row of probability matrix/entropy
        P_mat[_sample, :] = _P_mat
        entropies[_sample] = _entropy

    return sigmas, entropies, P_mat

# Trash

#def make_layers(all_layer_sizes):
#    all_layers = [layers.Dense(all_layer_sizes[1],
#                    input_shape=(all_layer_sizes[0],), activation='sigmoid',
#                    kernel_initializer='glorot_uniform')]
#
#    for lsize in all_layer_sizes[2:-1]:
#        all_layers.append(layers.Dense(lsize,
#                          kernel_initializer='glorot_uniform'))
#
#    all_layers.append(layers.Dense(all_layer_sizes[-1], activation='linear', kernel_initializer='glorot_uniform'))
#
#    return all_layers
#
