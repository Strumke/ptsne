import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def plot_scatter(embedding, labels, color_palette=None, alpha=0.5, marker='.',
        label=None):
    unique_labels = set(labels)
    num_clusters = len(unique_labels)
    if color_palette is None:
        color_palette = sns.color_palette("hls", num_clusters)
    for _n, _l in enumerate(unique_labels):
        _rows = (labels == _l)
        try:
            _color = color_palette[_l]
        except IndexError:
            _color = color_palette[_n]
        if label is None:
            plt.plot(embedding[_rows, 0], embedding[_rows, 1],
                    marker, color=_color, label=_l, alpha=alpha)
        else:
            plt.plot(embedding[_rows, 0], embedding[_rows, 1],
            marker, color=_color, label=label, alpha=alpha)


def plot_kde(embedding, labels, color_palette=None, alpha=0.5):
    num_clusters = len(set(labels))
    if color_palette is None:
        color_palette = sns.color_palette("hls", num_clusters)
    for _n in range(num_clusters):
        _rows = (labels == _n)
        _cmap = sns.light_palette(color_palette[_n], as_cmap=True)
        sns.kdeplot(embedding[_rows, 0], embedding[_rows, 1],
                cmap=_cmap, shade=True, alpha=alpha, shade_lowest=False)
        centroid = embedding[_rows, :].mean(axis=0)
        plt.annotate('%s' % _n, xy=centroid, xycoords='data', alpha=0.5,
                     horizontalalignment='center', verticalalignment='center')

def plot_clusters(embedding, train_labels, pred=None, test_labels=None, ax=None):
    if ax is None:
        ax = plt.gca()
    num_clusters = len(set(train_labels))
    sns.scatterplot(x=embedding[:,0], y=embedding[:,1],
                    hue=train_labels,
                    palette=sns.color_palette("hls", num_clusters), ax=ax)

    if pred is not None and test_labels is not None:
        num_clusters = len(set(test_labels))
        markers = dict()
        for _l in set(test_labels):
            markers[_l] = '*'
        sns.scatterplot(x=pred[:,0], y=pred[:,1], markers=markers,
                        hue=test_labels, style=test_labels,
                        palette=sns.color_palette("hls", num_clusters), ax=ax)

if __name__ == "__main__":

    embedding = np.load("embeddings.npy")
    train_labels = np.load("train_labels.npy")
    pred = np.load("preds.npy")
    test_labels = np.load("test_labels.npy")

    # --- Plot result with unseen samples
    from make_plot import plot_kde, plot_clusters, plot_scatter
    plot_kde(embedding, train_labels)
    plot_scatter(pred, test_labels)
    plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
    plt.show()

    # --- Plot result with unseen samples
    plot_clusters(embedding, train_labels, pred, test_labels)
    plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
    plt.show()
