TARGET_DICT = {0: "dyed_lifted_polyps",
               1: "dyed_resection_margins",
               2: "esophagitis",
               3: "normal_cecum",
               4: "normal_pylorus",
               5: "normal_z_line",
               6: "polyps",
               7: "ulcerative_colitis"}

# --- For training and plotting
csv_file = "latent_features.csv"
num_outputs = 2
num_clusters = 8
alpha = num_outputs-1
perplexity = 50
model_name = "test_model.h5"
batch_size = 128
epochs = 10#0
save_results = True
show_plot = True

# --- For analysis of unseen data
pred_index = 8
n_neighbors = 20
