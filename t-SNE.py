import numpy as np
import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import functools
import tensorflow as tf
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping
from sklearn.model_selection import train_test_split

# --- My stuff
from helpers import kl_loss, make_train_generator
from ptsne_parameters import (csv_file, num_outputs, num_clusters, alpha,
        perplexity, model_name, batch_size, epochs, save_results, show_plot)
from make_plot import plot_kde, plot_clusters, plot_scatter

TARGET_DICT = {0: "dyed_lifted_polyps",
               1: "dyed_resection_margins",
               2: "esophagitis",
               3: "normal_cecum",
               4: "normal_pylorus",
               5: "normal_z_line",
               6: "polyps",
               7: "ulcerative_colitis"}


def get_data(csv_file, n_samples=None):

    inv_target_dict = {v: k for k, v in TARGET_DICT.items()}

    latent_features_dataset = csv_file
    df = pd.read_csv(latent_features_dataset, index_col=0)

    if n_samples is not None:
        df = df.sample(n=n_samples)

    features = df.keys()[0:-1]
    targets = df.target.values
    data = np.array(df[features])
    labels = np.array([inv_target_dict[_i] for _i in targets])

    return data, labels

def make_model(num_outputs):
    #all_layer_sizes = [num_inputs, 500, 500, 2000, num_outputs]
    #all_layers = make_layers(all_layer_sizes)
    inputs = tf.keras.Input(shape=(num_inputs,))
    next_layer = tf.keras.layers.Dense(500,
            activation=tf.nn.sigmoid)(inputs)
    next_layer = tf.keras.layers.Dense(500,activation=tf.nn.sigmoid,
            kernel_initializer=tf.keras.initializers.GlorotUniform(),
            )(next_layer)
    next_layer = tf.keras.layers.Dense(2000,activation=tf.nn.sigmoid,
            kernel_initializer=tf.keras.initializers.GlorotUniform(),
            )(next_layer)
    outputs=tf.keras.layers.Dense(num_outputs,activation=tf.keras.activations.linear,
            kernel_initializer=tf.keras.initializers.GlorotUniform(),
            )(next_layer)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)

    kl_loss_func = functools.partial(kl_loss, alpha=alpha,
        batch_size=batch_size)
    kl_loss_func.__name__ = 'KL-Divergence'

    optimiser = "adam"
    model.compile(optimizer=optimiser, loss=kl_loss_func, run_eagerly=True)
    print(model.summary())

    return model

def train_model(model, generator, num_samples, batch_size):
    steps_per_epoch = int(num_samples//batch_size)
    reduce_lr = ReduceLROnPlateau(monitor="loss", factor=0.2, patience=5,
            min_lr=0.001)
    early_stopping = EarlyStopping( monitor="loss", min_delta=0, patience=10,
            verbose=0, mode='auto', baseline=None, restore_best_weights=False)
    callbacks = [reduce_lr, early_stopping]

    # TODO: Hyperparameter tuning - or RBM
    model.fit(generator,
            steps_per_epoch=steps_per_epoch,
            epochs=epochs,
            callbacks=callbacks)
    return model

# --- Data
data, labels = get_data(csv_file)
train_data, test_data, train_labels, test_labels = train_test_split(data,
        labels, test_size=0.05)
num_samples, num_inputs = train_data.shape
print("Samples: ", num_samples)
print("Dimensionality: ", num_inputs)


# if pred is not None and test_labels is not None:
#        num_clusters = len(set(test_labels))
#        markers = dict()
#        for _l in set(test_labels):
#            markers[_l] = '*'
#        sns.scatterplot(x=pred[:,0], y=pred[:,1], markers=markers,
#                        hue=test_labels, style=test_labels,
#                        palette=sns.color_palette("hls", num_clusters), ax=ax)
# --- Plot embedding compared to non-parametric t-SNE
from sklearn.manifold import TSNE
tsne = TSNE(n_components=2, perplexity=perplexity, random_state=0)
tsne_embedding = tsne.fit_transform(train_data)
#plot_clusters(tsne_embedding, train_labels, ax=ax2)
num_clusters = len(set(train_labels))
sns.scatterplot(x=tsne_embedding[:,0], y=tsne_embedding[:,1],
                hue=train_labels,
                palette=sns.color_palette("hls", num_clusters))

plt.show()
